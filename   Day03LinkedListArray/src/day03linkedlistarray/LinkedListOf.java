/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03linkedlistarray;

import java.util.ArrayList;

/**
 *
 * @author AQ-37
 */
public class LinkedListOf<T> {
 
    private class Container {

        Container next;
        T value;

        public Container(Container next, T value) {
            this.next = next;
            this.value = value;
        }

    }
    Container start = null;
    Container end = null;
    int size;
    ArrayList< Container> items = new ArrayList<>();

    /**
     * *******************************************************************************
     */
    public void add(T value) {
        T containerValue = value;//SET THE VALUE OF THE NEW ELEMENT
        Container next = end;//SET THE NEXT OF THE NEW ELEMENT,ALLWAS WILL BE END.
        Container newItem = new Container(next, value);//CREATE THE NEW ELEMENT
        if (size == 0) {
            start = newItem;//IF IT IS THE FIRT ELEMENT IN THE ARRAY,LET THE START POINT TO IT.
        } else {
            items.get(size - 1).next = newItem;//IF IT IS NOT THE FIRST EELEMNT,LET THE LAST ELEMENT IN THE LINKEDARRAYlIST POINTTO IT AS NEXT ELEMENT

        }
        items.add(newItem);//ADD THE NEW ITEM TO THE LIST
        size++;//AND INCREASE THE SIZE BY ONE
    }

    public T get(int index) {
        return items.get(index).value;
    }

    /**
     * *******************************************************************************
     */
    public void insertValueAtIndex(int index, T value) {
        if (index > size) {//IF SUCH INDEX NOT FOUND:THROE AN EXCEPTION.
            throw new ArrayIndexOutOfBoundsException("insert index is not valid");
        }//ELSE:
        Container indexOldContainer = items.get(index);//1-STORE THE OLD ITEM OF THE GIVEN INDEX
        for (int i = size; i >= index; --i) {
            items.add(i, items.get(i - 1));//2-FOR THE GIVEN INDEX AND UP TO LAST ONE:MOVE EACH ELEMENT TO THE NEXT INDEX
        }
        Container indaxNewContainer = new Container(indexOldContainer, value);//3-CREATE THE VEW ITEM:THE (NEXT) POINTER WILL BE POINT TO THE OLDITEM IN THE GIVEN INDEX
        items.get(index - 1).next = indaxNewContainer;//LET THE(NEXT)POINTER OF THE ITEM BEFORE THE GIVEN INDEX POINT TO THE NEW INSERTED ITEM
        items.add(index, indaxNewContainer);//ADD THE NEW ITEM IN THE GIVEN INDEX OF THE ARRAY
        size++;//INCRESE THE SIZE BY ONE.
    }

    /**
     * *******************************************************************************
     */
    public void deleteByIndex(int index) {
        /*if (index>size-1) {//IF SUCH INDEX IS NOT FOUND:THRW AN EXCEPTION.index<0||
            throw new ArrayIndexOutOfBoundsException("index is not valid");
        }//ELSE:*/
        Container next=items.get(index).next;
         items.remove(index);//1-REMOVE THE SELECTED INDEX FROM THE ITEMS ARRAY
        if (index != 0) {//3-LINK BETWEEN THE ITEM BEFORE THE GIVEN INDEX WITH THE NEW ITEM(AFTER SIFTING)OF THAT INDEX(ESCAPE THIS STEP IF WE REACH INDEX 0).
            items.get(index - 1).next=next;
        for (int i = index; i <size - 2; i++) {//2-SHIFT ALL THE ITEMS AFTER THE GINVEN INDEX TO LEFT
            items.add(i, items.get(i + 1));
        }
        }
        else{
               for (int i = index; i <size -1; i++) {//2-SHIFT ALL THE ITEMS AFTER THE GINVEN INDEX TO LEFT
            items.add(i, items.get(i + 1));
        } 
        }       
        size--;//DECREASE THE SIZE BY ONE
        
    }

    /**
     * *******************************************************************************
     */
    public boolean deleteByValue(T value) {// delete first value found**/
        for (Container c : items) {//LOOP THROUGH THE LINKED LIST
            if (c.value.equals(value)) {//A-SUCH A VALUE IS FOUND,START DELETING PROCESS:
                int index = items.indexOf(c);//A.1-TAKE THE INDEX OF THE THE ITEM FOUND
                deleteByIndex(index);//A.2-CALL THE deleteByIndex FUNCTION TO REMOVE THAT INDEX.THE SIZE WILL BE DECREMENTED BY THAT FUNCTION.
                return true;//A.3-RETURN THE CONFIRMATION THAT THE VALUE HAS BEEN FOUND
            }
        }
        return false;//B-IF NOT FOUND:RETURN FALSE TO TELL THAT IT DOESNT FOUND IN THE LINKED LIST
    }

    /**
     * *******************************************************************************
     */
    public int getSize() {
        return size;
    }

   @Override
    public String toString() {
        String str = "[";
        for (int i=0;i<size ;++i) {
            str += items.get(i).value + (items.get(i).next == null ? "" : ",");
        }
        str += "]";
        return str;
    } // comma-separated values list

    /**
     * *******************************************************************************
     */
    public String[] toArray() { //RETURN A STRING ARRAY OF THE VALUES OF THE LINKEDLIST
       String[] toArray = new String[size];
        for (int i = 0; i < size; i++) {
        toArray[i]=(String) items.get(i).value;
        }
        return toArray ;       
    } // could be used for Unit testing

    public static void main(String[] args) {
        // TODO code application logic here
    }

}
   

