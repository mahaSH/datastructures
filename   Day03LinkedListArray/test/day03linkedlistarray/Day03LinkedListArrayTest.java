/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03linkedlistarray;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author AQ-37
 */
public class Day03LinkedListArrayTest {

    public Day03LinkedListArrayTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Day03LinkedListArray.
     */
    @Test
    public void testAddFive() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("What");
        instance.add("a");
        instance.add("nice");
        instance.add("weekend");
        assertEquals("[What,a,nice,weekend]", instance.toString());

    }

    @Test
    public void testAddFiveDeleteOne() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("What");
        instance.add("a");
        instance.add("nice");
        instance.add("weekend");
        instance.deleteByIndex(2);
        assertEquals("[What,a,weekend]", instance.toString());
    }

 @Test
    public void testAddFiveToArray() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("What");
        instance.add("a");
        instance.add("nice");
        instance.add("weekend");
      String []content = instance.toArray();
       assertArrayEquals("should be[What a nice weeked]", new String[] {"What","a","nice","weekend"}, content);
    }
    @Test
    public void testAddFiveDeleteOneGetSize() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("What");
        instance.add("a");
        instance.add("nice");
        instance.add("weekend");
        instance.deleteByIndex(0);
         instance.add("day");
        assertEquals(4, instance.getSize());         
    }
   @Test
    public void testAddFiveDeleteAll() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("What");
        instance.add("a");
        instance.add("bad");
        instance.add("weekend");
        
      instance.deleteByIndex(0);
        instance.deleteByIndex(1);
        instance.deleteByIndex(2);
        instance.deleteByIndex(3);
        assertEquals(0, instance.getSize()); 
        String []content = instance.toArray();
        assertArrayEquals("should be[]", new String[] {}, content);
        assertEquals("[]", instance.toString());
        
    }
}
