/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

class FIFOFullException extends Exception {

    FIFOFullException() {
        super();
    }
}

/**
 *
 * @author 6155202
 */
public class RollingPriorityFIFO {

    private class Item {
        // add constructor of your choice

        Item next;
        boolean priority;
        String value;

        private Item(Item next, String value, boolean b) {
            this.next = next;
            this.priority = false;
            this.value = value;
        }
    }
    private Item start, end;
    int itemsTotal, itemsCurrUsed;

    /* Parameter itemsTotal must be 5 or more, otherwise IllegalArgumentException
    * is thrown. Items are allocated and connected via next/prev pointer only
    * once - here, in the constructor. After that they are re-used.
     */
    public RollingPriorityFIFO(int itemsTotal) {
        if (itemsTotal < 5) {//1-CHECK THE SIZE:IF LESS THAN 5 THEN THROW ILLIGALaRGUMENT EXCEPTION
            throw new IllegalArgumentException("QUEUE SHOLD BE OF SIZE 5 AND MORE");
        }
        int counter = 1;
        Item item = null;
        Item firstItem = new Item(null, null, false);//CREATE THE FIRST ELEMENT
        item = firstItem;
        do {                                         //CREATE THE OTHER ELEMENTS
            Item newItem = new Item(null, null, false);
            item.next = newItem;                       //CONNECT BETWEEN THE ITEMS
            item = newItem;                              //KEEP THE ITEM CREATED ASIDE TO CONNECT IT WITH THE ITEM THAT WILL BE CREATED IN THE NEXT ITERATION
            counter++;
        } while (counter != itemsTotal);
        item.next = firstItem;                         //CREATE THE CONNECTION BETWEEN THE FIRST ELEMNT OF THE QUEUE AND THE LAST ELEEMENT
        start = firstItem;                             //START POINTER WILL POINT TO THE FRIST ELEMENT OF THE QUEUE
        end = firstItem;                               //END POINTER WILL POINT TO THE FRIST ELEMENT OF THE QUEUE
        itemsCurrUsed = 0;                            //NO ITEM HAS BEEN USED YET    
        this.itemsTotal = itemsTotal;
    }

    /**
     * ***********************************************************************************************************************************
     */
    public void enqueue(String value, boolean priority) throws FIFOFullException {// Places value in the next available Item. If FIFO is full throws exception.
        if (itemsCurrUsed > itemsTotal) {//IF THE QUEUE IF FULL,THROW EXCEPTION
            throw new FIFOFullException();
        }//OTHERWISE:
        Item item = start;//TAKE THE ITEM WHERE THE START POINT
        item.value = value;//GIVE THE VALUE TO THAT ITEM
        item.priority = priority;//GIVE THE PRIORITY TO THAT ITEM
        start = item.next;//LET START POINT TO THE ITEM THAT FOLLOW
        itemsCurrUsed++;//ITEMS CURRENT USED NOE INCREMENTED BY ONE
    }

    /**
     * ***********************************************************************************************************************************
     */
    public String dequeue() {
        /* returns null if fifo is empty, if it is not emtpy then * priority=true items are sarched first* if none is found then non-priority item is returned*/

        if (start == end) {//CHECK IF THE START POINTER AND THE END POINTER POIN TO THE SAME ELEMENT, WHICH MEANS THAT THE QUEUE IS EMPTY
            return null;//IF EMPTY:RETURN NULL
        }
        String returnedValue = null;
        Item item = end;             //GET THE TAIL OF THE QUEUE,WHERE EEND POINTS
        returnedValue = item.value;  //THIS IS THE FIRST VALUE TO BE DEQUEUED IN OUR FIFIO,KEEP ITS VALUE TO BE RETURNED
        item.value = null;           //REMOVE THE DATAT FROM THIS ITEM.
        itemsCurrUsed--;           //DECREMENT THE COUNTER OF THE USED ITEM IN FIFO
        end = item.next;             //LET THE POINTER POINT TO THE NEXT ELEMENT
        return returnedValue;      //RETURN THE VALUE 
    }

    public int size() {
        return itemsCurrUsed;
    } // current FIFO size

    public int sizeMax() {
        return itemsTotal;
    } // maximum FIFO size

    /**
     * ***********************************************************************************************************************************
     */
    public String[] toArray() {// Returns array of Strings of all items in FIFO.
        String[] result = new String[itemsCurrUsed];
        int count = 0;
        Item current = end;
        while (current != start) {
            result[count++] = current.value;
            current = current.next;
        }
        return result;
    }

    /**
     * ***********************************************************************************************************************************
     */
    public String[] toArrayOnlyPriority() {// Returns array of String only of priority items in FIFO.
        if (start == end) {//CHECK IF THE START POINTER AND THE END POINTER POIN TO THE SAME ELEMENT, WHICH MEANS THAT THE QUEUE IS EMPTY
            return null;//IF EMPTY:RETURN NULL
        }
        Item item = end; //GET THE LAST ITEM WHERE END POINTS
        int counter = 0;//INITIALIZE THE COUNTER THAT WILL GIVE THE NUMBER OF THE PRIORITY ITEMS IN OUR FIFO

        for (int i = 0; i < itemsCurrUsed; i++) {//FIND THE NUMBER OF PRIORITY ITEMS
            if (item.priority == true) {
                counter++;
            }
            item = item.next;
        }
        String[] returnedArray = new String[counter];//INITIALIZE THE ARRAY OF THE PRIORITY ITEMS
        // System.out.println(counter);
        int count = 0;
        item = end;
        while (item != start) {//LOOP AGAIN BETWEEN THE HEAD AND TE TAIL OF THE FIFO,PUT ALL THE PRIORITY VALUES IN THE ARRAY
            boolean p = item.priority;
            if (p == true) {
                returnedArray[count] = item.value;
                start.next = item;//MVE THE PRIORITY ELEMENT TO SOMEWHERE BEHIND START POINTER
                count++;
            }
            item = item.next;  //STEP TO THE NEXT ITEM           
        }
        return returnedArray;      //RETURN THE VALUE     
    }

    /**
     * ***********************************************************************************************************************************
     */
    @Override
    public String toString() {
        return String.format("[%s]", String.join(",", toArray()));
    }
}
