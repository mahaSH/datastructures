/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 6155202
 */
public class RollingPriorityFIFOTest {

    public RollingPriorityFIFOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of enqueue method, of class RollingPriorityFIFO.
     */
    /*@Test(expected = IllegalArgumentException.class)
    public void testNumberOfItemsLessThanFive() {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(3);
            }  */
    @Test
    public void testEnqueueTheeItems() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Take", false);
        fifo.enqueue("it", false);
        fifo.enqueue("easy", true);
        assertArrayEquals("Array not the same (1)",
                new String[]{"Take", "it", "easy"}, fifo.toArray());
    }

    @Test//(expected = IllegalArgumentException.class)
    public void testEnqueueTheeItemsDequeueThree() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Take", false);
        fifo.enqueue("it", false);
        fifo.enqueue("easy", true);
        fifo.dequeue();
        fifo.dequeue();
        fifo.dequeue();
        assertArrayEquals("Array not the same (1)",
                new String[]{}, fifo.toArray());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateLessThanFive() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(3);
        fifo.enqueue("Take", false);
        fifo.enqueue("it", false);
        fifo.enqueue("easy", true);
        fifo.dequeue();
        fifo.dequeue();
        fifo.dequeue();
        assertArrayEquals("Array not the same (1)",
                new String[]{}, fifo.toArray());
    }

    @Test(expected = FIFOFullException.class)
    public void testInsertMoreThanFifoLength() {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        try {
            fifo.enqueue("Take", false);
            fifo.enqueue("it", false);
            fifo.enqueue("easy", true);
            fifo.enqueue("life", true);
            fifo.enqueue("is", true);
            fifo.enqueue("beautiful", true);
            fail("FAIL");
        } catch (FIFOFullException ex) {
            //DO NOTHING
        }
    }

    @Test
    public void testInsertThreeReturnPriority() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Take", false);
        fifo.enqueue("it", false);
        fifo.enqueue("easy", true);
        fifo.toArrayOnlyPriority();
        assertArrayEquals("Array not the same (1)",
                new String[]{"easy"}, fifo.toArrayOnlyPriority());
    }

    @Test
    public void testInsertThreeDequeOneReturnPriority() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Take", true);
        fifo.enqueue("it", false);
        fifo.enqueue("easy", true);
        fifo.dequeue();
        fifo.toArrayOnlyPriority();
        assertArrayEquals("Array not the same (1)",
                new String[]{"easy"}, fifo.toArrayOnlyPriority());
    }
    @Test
    public void testGetFifoSiae() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        assertEquals(5, fifo.size());
    }
}
