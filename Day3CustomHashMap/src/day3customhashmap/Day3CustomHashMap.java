/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day3customhashmap;

/**
 *
 * @author 6155202
 */
public class Day3CustomHashMap {

    private class Container {

        public Container(String key, String value) {
            this.key = key;
            this.value = value;
        }

        Container next;
        String key;
        String value;
    }

    Container[] hashTable = new Container[7];
    private int totalItems;

    private int computeHash(String input) {
        int hash = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            hash <<= 1;
            hash += c;
        }
        hash += input.length();
        return Math.abs(hash);
    }

    String getValue(String key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    Container getEntryByKey(Container start, String key) {
        Container current = start;
        while (current != null) {
            if (current.key.equals(key)) {
                return current;
            }
            current = current.next; // move on to next item
        }
        return null;
    }

    void putValue(String key, String value) {
        /// LATER: expand hashTable by about *2 when totalItems > 3*hashTable.length
        int index = computeHash(key) % hashTable.length;
        Container found = getEntryByKey(hashTable[index], key);
        if (found != null) {
            found.value = value; // set new value
        } else {
            // insert at the beginnig
            Container newItem = new Container(key, value);
            newItem.next = hashTable[index];
            hashTable[index] = newItem;
            totalItems++;
        }
    }

    boolean deleteByKey(String key) {
        int index = computeHash(key) % hashTable.length;
        Container found = getEntryByKey(hashTable[index], key);
        if (found == null) {
            return false;
        } else {//IF SUCH A KEY FOUND:DELETE IT
            Container current = hashTable[index];
            while (current.next != found) {
                current = current.next; // move on to next item
            }
            current.next = found.next;
            totalItems--;
            return true;
        }
    }

    int getSize() {
        return totalItems;
    }

    boolean hasKey(String key) {
        int index = computeHash(key) % hashTable.length;
        //Container found = getEntryByKey(hashTable[index], key);
        Container current = hashTable[index];
        while (current!= null) {
            if (current.key ==key)return true;// move on to next item
        }
        return false;
    }

    public void printDebug() {
        for (int index = 0; index < hashTable.length; index++) {
            System.out.println("Entry: " + index);
            Container current = hashTable[index];
            while (current != null) {
                System.out.printf("- Key %s: %s\n", current.key, current.value);
                current = current.next; // move on to next item
            }
        }
    } // print hashTable content, one entry per line, with all items in it.

    @Override
    public String toString() {
        throw new UnsupportedOperationException("Not implemented");
    } // comma-separated values->key pair list
    // e.g. [ Key1 => Val1, Key2 => Val2, ... ]

    public static void main(String[] args) {
        int mod = 7;
        String[] keysArray = {"witness", "cherry", "barrier", "clerk", "scrap",
            "silver", "refuse", "accurate", "pace", "fool"};
        String[] valuesArray = {"account", "force", "marathon", "costume",
            "throw", "fraction", "apple", "owl", "forward", "gem"};

        Day3CustomHashMap map = new Day3CustomHashMap();

        for (int i = 0; i < keysArray.length; i++) {
            map.putValue(keysArray[i], valuesArray[i]);
        }
        map.printDebug();

        map.putValue("silver", "gold");

        map.printDebug();
        map.deleteByKey("silver");
        map.printDebug();
        boolean has=map.hasKey("scrap");
        System.out.println(has);
    }
}
