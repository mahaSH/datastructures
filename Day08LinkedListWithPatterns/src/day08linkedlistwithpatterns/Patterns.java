/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08linkedlistwithpatterns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author AQ-37
 */
public class Patterns implements Iterable<Integer> {
 public interface ContainerModificationObserver {
        public void containerModified(Container container, Action action);
        public enum Action { Added,Removed };
    }

    private ArrayList<ContainerModificationObserver> observerList = new ArrayList<>();

    void addObserver(ContainerModificationObserver observer) {
        observerList.add(observer);
    }

    void callObserver(Container container, ContainerModificationObserver.Action action) {
        for (ContainerModificationObserver observer : observerList) {
            observer.containerModified(container, action);
        }
    }
    @Override
    public Iterator<Integer> iterator() {
        return new LinkedListArrayIterator(toArray());
    }
    
    
   class LinkedListArrayIterator implements Iterator<Integer>{
       private int[] toArray;
        private int currIndex;
        

        private LinkedListArrayIterator(int[] toArray) {
            this.toArray = toArray;
        }
        @Override
        public boolean hasNext() {
           return currIndex < toArray.length;
        }

        @Override
        public Integer next() {
             return toArray[currIndex++];
        }
       
   }

    class Container {

        public Container(int value) {
            this.value = value;
        }

        
        int value;
        Container next;
    }

    Container start, end;
    int size;
    public void add(int value) {
        if (size == 0) { // list empty, start & end are null
            start = new Container(value);
            end = start;
            System.out.println(size);
            
            size++;
            callObserver(end, ContainerModificationObserver.Action.Added);
        } else { // at least one item, end points to the last one
            Container cont = new Container(value);
            end.next = cont; // attach new container
            end = cont; // make end point to the new last item
            size++;
            callObserver(end, ContainerModificationObserver.Action.Added);
        }
    }

    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        // optimize access to the last item
        if (index == size - 1) {
            return end.value;
        }
        //
        Container current = start;
        for (int count = 0; count < index; count++) {
            current = current.next;
        }
        return current.value;
    }

    public void insertValueAtIndex(int index, int value) {
        if (index < 0 || index > size) { // equal is okay: insert at the end, same as add
            throw new IndexOutOfBoundsException();
        }
        //
        if (index == size) { // either list is empty or insert at the end, same as add
            add(value);
            callObserver(end, ContainerModificationObserver.Action.Added);
            return;
        }
        Container newContainer = new Container(value);
        if (index == 0) {
            newContainer.next = start;
            start = newContainer;
           
            size++;
             callObserver(end, ContainerModificationObserver.Action.Added);
        } else {
            Container current = start;
            // find the item just before the item we want to insert
            for (int count = 0; count < index - 1; count++) {
                current = current.next;
            }
            newContainer.next = current.next;
            current.next = newContainer;
            size++;
             callObserver(end, ContainerModificationObserver.Action.Added);
        }
    }

    public void deleteByIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        //
        if (index == 0) { // special case
            start = start.next;
            size--;
            if (size == 0) { // not possible to test it with Unit tests, usually
                callObserver(end, ContainerModificationObserver.Action.Removed);
                end = null;
            }
            return;
        }
        //
        Container current = start;
        // find the item just before the item we want to delete
        for (int count = 0; count < index - 1; count++) {
            current = current.next;
        }
        current.next = current.next.next;

        if (index == size - 1) {
            // removing the last item
            end = current;
        }
        
        size--;
        callObserver(end, ContainerModificationObserver.Action.Removed);
    }

    public boolean deleteByValue(int value) { // delete first value found
        int count = 0;
        Container current = start;
        while (current != null) {
            if (current.value==value) {
                deleteByIndex(count);
                return true;
            }
            current = current.next;
            count++;
        }
        return false;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() { // comma-separated values list
         int[] data = toArray();
        String[]textArray = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            textArray[i] = data[i]+"";
        }
        return String.format("[%s]", String.join(",", textArray));
    }

  public int[] toArray() { // could be used for Unit testing
        int[] result = new int[size];
        //System.out.println(size);
        int count = 0;
        Container current = start;
        while (current != null) {
            result[count] = current.value;
            count++;
            current = current.next;
        }
        return result;
    }
  /*public int[] getSorted(int[]toArray){
      Integer[]values=toArray;
       Collections.sort(values, comparatorInt);
  }
 final static Comparator<Integer> comparatorInt = new Comparator<Integer>() {
        @Override
        public int compare(Integer p1, Integer p2) {
            return p2 - p1;
        }
    };*/
}

    

