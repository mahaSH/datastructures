/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08linkedlistwithpatterns;

import day08linkedlistwithpatterns.Patterns.ContainerModificationObserver;

/**
 *
 * @author AQ-37
 */
public class Day08LinkedListWithPatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      Patterns pat = new Patterns();
      pat.addObserver(((container, action) -> {
        
            System.out.println("Modification perfomed action: " + action +".Current number of containers is "+ pat.size+".The value "+container.value+" has been "+action);
            
       }));
       /*pat.addObserver(new Patterns.ContainerModificationObserver() {
          @Override
          public void containerModified(Patterns.Container container, ContainerModificationObserver.Action action) {
           System.out.println("(ALPHA) Modification perfomed action=" + action + " on node: " + pat); 
          }
      });*/
        for (int n : new int[]{ 10, 7, 12, 15, 8, 5, 3, 11 } ) {
            pat.add(n);
        }
        pat.deleteByIndex(3);
   
       for (int v : pat) {
            System.out.print(v + ", ");
        }
        
    }
    
}
