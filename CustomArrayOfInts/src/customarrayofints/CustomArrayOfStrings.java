/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customarrayofints;

/**
 *
 * @author 6155202
 */
public class CustomArrayOfStrings {
    


    private String[] data = new String[1]; // only grows by doubling size, never shrinks
    private int size = 0;

    private void ensureNewSize(int newSize) {
        if (data.length < newSize) { // need more space
            String[] newData = new String[data.length * 2]; // double the size
            for (int i = 0; i < size; i++) {
                newData[i] = data[i];
            }
            data = newData;
        } else if (size * 4 < data.length && data.length >= 8) { // shrink it
            // TODO: shrink when less than 1/4 is used, but never smaller than 4 elements
            int newLength = data.length / 4;
            System.out.printf("Notice: shrinking data from %d to %d\n", data.length, newLength);
            String[] newData = new String[newLength];
            // copy data
            for (int i = 0; i < size; i++) {
                newData[i] = data[i];
            }
            data = newData;
        }
    }

    public int size() {
        return size;
    }

    public void add(String value) {
        ensureNewSize(size + 1);
        data[size] = value;
        size++;
    }

    public void deleteByIndex(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = index + 1; i < size; i++) {
            data[i - 1] = data[i];
        }
        size--;
        ensureNewSize(size);
    }

    public boolean deleteByValue(String value) {  // delete first value matching
        for (int i = 0; i < size; i++) {
            if (data[i].equals(value)) {
                deleteByIndex(i);
                return true;
            }
        }
        return false; // no match found
    }

    public void insertValueAtIndex(String value, int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        ensureNewSize(size + 1);
        for (int i = size; i > index; i--) {
            data[i] = data[i-1];
        }
        data[index] = value;
        size++;
    }

    public void clear() {
        size = 0;
    }

    public String get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[index];
    }
    public String[] getSlice(int startIdx, int length) {
        if (startIdx < 0 || startIdx >=size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (length < 0 || startIdx + length >size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        String [] result = new String[length];
        for (int i = startIdx; i < startIdx + length; i++) {
            result[i - startIdx] = data[i];
        }
        return result;
    }

    @Override
    public String toString() {
        // returns String similar to: [3, 5, 6, -23]
        String result = "[";
        for (int i = 0; i < size; i++) {
            String val = data[i];
            result += (i == 0) ? "" : ", ";
            result += val;
        }
        result += "]";
        return result;
    }




    public static void main(String[] args) {
        // TODO code application logic here
    }   
}
