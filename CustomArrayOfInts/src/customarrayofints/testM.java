/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customarrayofints;

import java.util.Scanner;

/**
 *
 * @author AQ-37
 */
public class testM {

    public static void main(String[] args) {
        int[] data = new int[1]; // only grows by doubling size, never shrinks
        int size = 0;
        CustomArrayOfIntsM array = new CustomArrayOfIntsM(data,size);
        int x;
        //size function test
        System.out.println(array.size());
        //TOSTRING FUNCTION TEST
        System.out.println(array.toString());
        //add function test
        System.out.println("enter value to be added");
        Scanner input=new Scanner(System.in);
        
        x=input.nextInt();
        input.nextLine();
        array.add(x);
        x=input.nextInt();
        input.nextLine();
        array.add(x);
        x=input.nextInt();
        input.nextLine();
        array.add(x);
        System.out.println("Adding function"+array.toString());
        //check get function
        System.out.println("which index do you want to retrieve?");
        x=input.nextInt();
        input.nextLine();
        int value=array.get(x);
        System.out.println("value at index "+x+" is"+value);
        //check deleteByIndex function
        //array.deleteByIndex(2);
        //check insertValueAtIndex fuction
        System.out.println("what value do you want to insert?");
        int num=input.nextInt();
        input.nextLine();
        System.out.println("in which index  do you want to insert?");
         int index=input.nextInt();
        input.nextLine();
        array.insertValueAtIndex(num,index);
       System.out.println("Insert function"+array.toString());
        //check clear function
        array.clear();
        System.out.println("clear function"+array.toString());
        array.deleteByIndex(0);
        System.out.println("dalata function"+array.toString());
        
    }
}
