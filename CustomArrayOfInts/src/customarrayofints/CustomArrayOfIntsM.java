/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customarrayofints;

/**
 *
 * @author AQ-37
 */
public class CustomArrayOfIntsM {

    private int[] data = new int[1]; // only grows by doubling size, never shrinks
    private int size = 0;

    public CustomArrayOfIntsM(int[]data,int size) {
        this.data=data;
        this.size=size;
        
    }

    
    
    public int size() {
        return data.length;
    }

    /**
     * *******************************************************************************************
     */
    public void add(int value) {
        int length = data.length;
        int[] old = new int[length];
        if (size< length) { //IF THE ARRAY IS FULL:
            for (int i = 0; i < size; i++) {//MOVE THE CONTENTS OF THE ARRAY TO A TEMPORARY STORAGE
                old[i] = data[i];
            }
            data = new int[length * 2];//INCREASE (DUPLICATE) THE LENGTH OF THE ARRAY
            for (int i = 0; i < size; i++) {//RETRIEVE THE OLD DATA
                data[i] = old[i];
            }
            data[size] = value;//ADD THE NEW VALUE TO THE UPDATED ARRAY
            size++;//INCREASE THE SIZE BY ONE
        } else {//IF THERE IS AN EMPTY SPACE:
            for (int i = 0; i < data.length; i++) {//FIND THE FIRST UNUSED INDEX THEN INSERT THE VALUE IN IT
                if (data[i] == 0) {
                    data[i] = value;
                    ;
                    break;
                }
            }
        }//END OF ELSE STAATEMENT

    }//END OF ADD FUNCTION

    /**
     * *******************************************************************************************
     */
    public void deleteByIndex(int index) {
        try {
             int[] old = new int[data.length];//TEMPORARY STORAGE
             for (int i = 0; i < data.length; i++) {
              old[i]=data[i];  
            }
             int[] data=new int[old.length-1];
            for (int i = index + 1; i < data.length; i++) {
                old[i - 1] = old[i];//MOVE THE CONTENTS OF EACH INDEX FOLLOW THE ONE WE WANT TO DELETE TO THE PREVIIOUS INDEX
            }
            for (int i = index + 1; i < old.length; i++) {
                data[i] = old[i];//MOVE THE CONTENTS OF EACH INDEX FOLLOW THE ONE WE WANT TO DELETE TO THE PREVIIOUS INDEX
            }
            
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("wrong index to be deleted has been entered");
        }
    }

    /**
     * *******************************************************************************************
     */
    public void deleteByValue(int value) {// delete first value matching
        int found = -1;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == value) {
                found = i;
                continue;
            }
        }
        deleteByIndex(found);
    }

    /**
     * *******************************************************************************************
     */
    public void insertValueAtIndex(int value, int index) {
        int length = data.length;
        int[] old = new int[length];
        if (size >= length) { //IF THE ARRAY IS FULL:
            for (int i = 0; i < size; i++) {//MOVE THE CONTENTS OF THE ARRAY TO A TEMPORARY STORAGE
                old[i] = data[i];
            }
            data = new int[length * 2];//INCREASE (DUPLICATE) THE LENGTH OF THE ARRAY
            for (int i = 0; i < size; i++) {//RETRIEVE THE OLD DATA
                data[i] = old[i];
            }
            for (int i = length-1; i > index; i--) {
                data[i + 1] = data[i];//SHIFT ALL THE ELEMENTS THAT FOLLOW THE DESIRED INDEX BY ONE TO RIGHT
            }

        } else {
            for (int i = length -2; i >= index; i--) {
                data[i + 1] = data[i];//SHIFT ALL THE ELEMENTS THAT FOLLOW THE DESIRED INDEX BY ONE TO RIGHT
            }

        }
        data[index] = value;//PUT THE SENT VALUE IN THE SENT INDEX
    }

    /**
     * *******************************************************************************************
     */
    public void clear() {
        for (int i = 0; i < data.length; i++) {
            data[i] = 0;
        }
    }

    /**
     * *******************************************************************************************
     */
    public int get(int index) {
        return (data[index]);
    }

    /**
     * *******************************************************************************************
     */
    public int[] getSlice(int startIdx, int length) {
        int[] subData = new int[length];
        for (int i = startIdx; i < length; i++) {
            subData[i] = data[i];
        }
        return subData;
    }

    /**
     * *******************************************************************************************
     */
    @Override
    public String toString() {
        String s=""; 
        for (int i = 0; i < size; i++) {
          s = s + data[i] + ((i == size - 1) ? " " : ",");
        }
        System.out.println(s);
        return String.format("[%s]", s);
    } // returns String similar to: [3, 5, 6, -23]  

    /**
     * *******************************************************************************************
     */
    
}
