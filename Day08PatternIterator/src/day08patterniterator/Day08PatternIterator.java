/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08patterniterator;

import java.util.Arrays;

/**
 *
 * @author 6155202
 */
public class Day08PatternIterator {

    /**
     * @param args the command line arguments
     */
   
        public static void main(String[] args) throws DuplicateValueException {
        TreeAgain tree = new TreeAgain();
        for (int n : new int[]{ 10, 7, 12, 15, 8, 5, 3, 11 } ) {
            tree.put(n);
        }
        System.out.println("Sum of all values: " + tree.getSumOfAllValues());
        System.out.println("Values in order: " + Arrays.toString(tree.getValuesInOrder()));
    }
    
}
