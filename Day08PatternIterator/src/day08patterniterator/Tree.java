package day08patterniterator;

import java.util.Arrays;
import java.util.Iterator;

class DuplicateValueException extends Exception {

    public DuplicateValueException() {
    }
}

// tree of unique values
class TreeAgain implements Iterable<Integer> {

    @Override
    public Iterator<Integer> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class NodeOfInt {

        NodeOfInt left, right;
        int value;
    }

    NodeOfInt root;
    private int nodesCount;

    void put(int value) throws DuplicateValueException {
        if (root == null) { // tree is empty
            NodeOfInt node = new NodeOfInt();
            node.value = value;
            root = node;
            nodesCount++;
            return;
        }
        NodeOfInt currNode = root;
        while (true) {
            if (currNode.value == value) {
                throw new DuplicateValueException();
            }
            if (currNode.value > value) { // go left
                if (currNode.left == null) {
                    NodeOfInt node = new NodeOfInt();
                    node.value = value;
                    currNode.left = node;
                    nodesCount++;
                    return;
                } else { // there is a left node
                    currNode = currNode.left;
                    // continue the loop
                }
            } else { // go right
                if (currNode.right == null) {
                    NodeOfInt node = new NodeOfInt();
                    node.value = value;
                    currNode.right = node;
                    nodesCount++;
                    return;
                } else { // there is a right node
                    currNode = currNode.right;
                    // continue the loop
                }
            }
        }
    }

    private int getSumOfSubNodes(NodeOfInt node) {
        if (node == null) return 0;
        return node.value + getSumOfSubNodes(node.left) + getSumOfSubNodes(node.right);
    }
    
    int getSumOfAllValues() {
        return getSumOfSubNodes(root);
    }

    private void collectValuesInOrder(NodeOfInt node) {
        if (node == null) return;
        collectValuesInOrder(node.left);
        resultArray[resultIndex++] = node.value;
        collectValuesInOrder(node.right);
    }
    
    private int[] resultArray;
    private int resultIndex;
    
    int [] getValuesInOrder() { // from smallest to largest, no sorting
        resultArray = new int[nodesCount];
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }
    
    /*
    void boolean hasValue(int value) { }
    
    void boolean remove(int value) { }
    
    int getValuesCount() { }
     */

}
