/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03BigTest;

import java.util.Scanner;

public class day03BigTest {
    static int getIfExistGentle(int[][]data,int row,int col){
        if(row>=data.length||row<0) return 0;
        if(col>=data[row].length||col<0) return 0;
        return data[row][col];
    }
    

    static int sumOfCross(int[][] data, int row, int col) {
      int sum=getIfExistGentle(data,row,col)+
              getIfExistGentle(data, row-1, col)+
              getIfExistGentle(data, row+1, col)+
              getIfExistGentle(data, row, col-1)+
              getIfExistGentle(data, row, col+1);
              return sum;
    }
     static void print2D(int[][] data) {
        int maxChars = 1;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                int len = (data[row][col] + "").length();
                maxChars = len > maxChars ? len : maxChars;
            }
        }
        for (int row = 0; row < data.length; row++) {
            System.out.print("[");
            for (int col = 0; col < data[row].length; col++) {
                System.out.printf("%s%" + maxChars + "d", col == 0 ? "" : ", ", data[row][col]);
            }
            System.out.println(" ]");
        }
    }

    

    public static void main(String[] args) {
        int[][] data2D =new int[20][10]; /*{
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},};*/
        //TASK 1:find the crossSum of element 2,2
        int sum = sumOfCross(data2D, 2, 2);
        System.out.println(sum);
        int small=data2D[0][0];
        int smRaw=0;
        int smCol=0;
        //TASK2:find the smallest sum of crossSum in the array
        for (int i = 0; i < data2D.length; i++) {
            for (int j = 0; j < data2D[i].length; j++) {
                if(sumOfCross(data2D, i,j)<small){
                 small=  sumOfCross(data2D, i,j);
                 smRaw=i;
                 smCol=j;
                 
                }
            }

        }
        /*TASK3:Create an integer array data2Dsums of identical size to data2D where
        each element of it is the cross-sum of the element in the original array.   
        then print out the original, and print out the new array*/
        //1-CREATE THE NEW ARRAY
        int[][] b = new int[data2D.length][];
        for (int i = 0; i < data2D.length; i++) {
               b[i]=new int [data2D[i].length];
             }
        //MAKE THE CALCULATION AND FILLING    
        for (int i = 0; i < data2D.length; i++) {
            for (int j = 0; j < data2D[i].length; j++) {
                 b[i][j]=sumOfCross(data2D, i,j);
                }
            }
        print2D(data2D);
        /*TASK3:Ask user for integer value. In data2D array find all pairs of
        values whose sum is the value entered by user*/
        System.out.println("please enter value");
        Scanner scan = new Scanner(System.in);
        int s = scan.nextInt();
        boolean found=false;
        int counter=0;
         for (int i = 0; i < data2D.length; i++) {
            for (int j = 0; j < data2D[i].length; j++) {
                   for (int k = 0; k < data2D.length; k++) {
                       for (int l = 0; l < data2D[i].length; l++){
                           counter++; 
                          if(k!=i&&l!=j) {
                               
                              if(data2D[i][j]+data2D[k][l]==s){
                                  
                                  found=true;
                                 /* System.out.printf("%d at[%d,%d]+ %d at[%d,%d] gives %d counter=%d\n"
                                          + "",data2D[i][j],i,j,data2D[k][l],k,l,s,counter);*/
                              }
                          }
                          
                       }
                       }
            }

        }
         /*if (found==false) System.out.println("No value found!!");*/
         System.out.printf("counter= %,d \n",counter);
    }//END OF MAIN CLASS
    

}
