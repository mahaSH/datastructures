/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07trianglebuildfractal;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author AQ-37
 */
public class trianglePanel extends javax.swing.JPanel {

    /**
     * Creates new form trianglePanel
     */
    public trianglePanel() {
        //initComponents();
    }

     protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        int fromX = 10, fromY = getHeight() - 20, toX = getWidth() - 10, toY = getHeight() - 20;
        g2d.drawLine(fromX, fromY, toX, toY); // draw baseline just once
        drawFractal(g2d, fromX, fromY, toX, toY);
    }

    private void drawFractal(Graphics2D g2d, int fromX, int fromY, int toX, int toY) {
        int aX = 0, aY = 0, bX = 0, bY = 0, cX = 0, cY = 0;
        int len=(bx-ax)*(bx-a)
        // 5 cases of triangle position
        if (fromX < toX && fromY > toY) { // case 1
            aX = fromX + len;
            bX = toX - len;
            aY = fromY - Math.abs(toY - fromY)/3;
            bY = toY + Math.abs(toY - fromY)/3;
            //
            cX = bX - len;
            cY = bY;

        } else if (fromX < toX && fromY < toY) { //case 2
            aX = fromX + len;
            bX = toX - len;
            aY = fromY + Math.abs(toY - fromY)/3;
            bY = toY - Math.abs(toY - fromY)/3;
            //
            cX = aX + len;
            cY = aY;
        } else if (fromX > toX && fromY > toY) { // case 3
            aX = fromX-len;
            bX =  toX - len;
            aY =  fromY + Math.abs(toY - fromY)/3;
            bY = toY - len;
            //
            cX = aX - len;
            cY = aY;
        } else if (fromX > toX && fromY < toY) { // case 4
            aX = fromX-len;
            bX =  toX+len;
            aY =  fromY +Math.abs(toY - fromY)/3;
            bY = toY - len;
            //
            cX = bX + len;
            cY = bY;
        } else {
            aX = fromX + len;
            bX = toX - len;
            aY = fromY;
            bY = toY;
            cX = (fromX+toX)/2; // middle
            int base = Math.abs(fromX-toX)/3;
            int height = (int)(base* Math.sqrt(3)/2);
            cY = bY-height;
        }
        g2d.drawLine(aX, aY, cX, cY);
        g2d.drawLine(bX, bY, cX, cY);
        // non-recusive case
        // if distance between from & to is less than 5 pixels - stop
        if (Math.sqrt((fromX - toX)*(fromX - toX) + (fromY - toY)*(fromY - toY)) < 5) return;
        // recursion
        drawFractal(g2d, fromX, fromY, aX, aY);
       //drawFractal(g2d, aX, aY, cX, cY);
        drawFractal(g2d, cX, cY, bX, bY);
        drawFractal(g2d, bX, bY, toX, toY);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        canvas1 = new java.awt.Canvas();

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, 586, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(109, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Canvas canvas1;
    // End of variables declaration//GEN-END:variables
}
