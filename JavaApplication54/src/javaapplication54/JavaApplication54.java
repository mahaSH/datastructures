/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication54;

import java.util.Random;

/**
 *
 * @author 6155202
 */
public class JavaApplication54 {

    static  int[] sort(int data[]){
         boolean reversed = true;
        int store;
        Random rand = new Random();
         for (int i = 0; i < data.length - 1; i++) {
             data[i]=(int)(Math.random()*1000);
         }
        while (reversed == true) {
            reversed = false;
            for (int i = 0; i < data.length - 1; i++) {
                if (i == (data.length) - 2) {
                    continue;
                }
                if (data[i] > data[i + 1]) {
                    store = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = store;
                    reversed = true;
                }
            }
        }
        return data;
    }
    static void print(int[] data){
       System.out.printf("[");
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%d %s", data[i], (i == data.length - 1) ? "" : ",");
        }
        System.out.printf("]");  
    }
    public static void main(String[] args) {
        int[] data = new int[10];
        System.out.println("array1");
        sort(data);
        print(data);
        System.out.println("array2");
        int[] data2=new int[100];
        sort(data2);
        print(data2);
        System.out.println("array3");
        int[] data3=new int[1000];
        sort(data3);
        print(data3);
       
       
    }

}
