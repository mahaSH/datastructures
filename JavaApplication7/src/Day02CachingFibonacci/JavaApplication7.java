/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day02CachingFibonacci;

import java.util.ArrayList;

class FibCached {

	FibCached() {
		fibsCached.add(0); // #0
		fibsCached.add(1); // #1
	}

	private ArrayList<long> fibsCached = new ArrayList<>();
	private fibsCompCount = 2;
	// in a correct caching implementation fibsCompCount will end up the same as fibsCached.size();

	public long getNthFib(int n) {
            long fab=0;
            if(n<fibsCached.size()){
                fab=fibsCached.get(n);
            }
            else{
                fab=computeNthFib(n);
                return fab;
        }
	
	// You can find implementation online, recursive or non-recursive.
	// For 100% solution you should use values in fibsCached as a starting point
	// instead of always starting from the first two values of 0, 1.
	private long computeNthFib(int n) { ... }
	
	// You are allowed to add another private method for fibonacci generation
	// if you want to use recursive approach. I recommend non-recursive though.

	// How many fibonacci numbers has your code computed as opposed to returned cached?
	// Use this in your testing to make sure your caching actually works properly.
	public int getCountOfFibsComputed() { ... }

	@Override
	String toString() { ... } // returns all cached Fib values, comma-space-separated
	
}
public class JavaApplication7 {
    
}
