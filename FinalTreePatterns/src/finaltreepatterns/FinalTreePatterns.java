/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

/**
 *
 * @author 6155202
 */
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 6155202
 */
class DuplicateValueException extends Exception {

    public DuplicateValueException() {
    }
}

/**
 * ************************************************************************************
 */
class Pair<K, V> {

    K key;
    V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

}

/**
 * ************************************************************************************
 */
class TreeStringIntSet implements Iterable {

    class TreeStringIntSetIterator implements Iterator<Pair> {

        private Pair[] getInOrder;
        private int currIndex;

        public TreeStringIntSetIterator(Pair[] getInOrder) {
            this.getInOrder = getInOrder;
        }

        @Override
        public boolean hasNext() {
            return currIndex < getInOrder.length;
        }

        @Override
        public Pair next() {
            return getInOrder[currIndex++];
        }

    }

    @Override
    public Iterator iterator() {
        return new TreeStringIntSetIterator(getInOrder());
    }

    //OBSERVER 
    public interface NodeModificationObserver {

        public void nodeModified(Node node, int value, Action action);

        public enum Action {
            New, Modified, Deleted
        };
    }

    private ArrayList<NodeModificationObserver> observerList = new ArrayList<>();

    private NodeModificationObserver observer;

    void addObserver(NodeModificationObserver observer) {
        observerList.add(observer);
    }

    void callObserver(Node node, int value, NodeModificationObserver.Action action) {
        for (NodeModificationObserver observer : observerList) {
            observer.nodeModified(node, value, action);
        }
    }
    //VISITOR:
    
    /**
     * ************************************************************************************
     */
    class Node {

        Node left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only
    }

    private Node root;
    private int nodesCount;

    /**
     * ************************************************************************************
     */
    void add(String key, int value) throws DuplicateValueException {
        if (root == null) { // tree is empty
            Node node = new Node();
            node.key = key;
            node.valuesSet.add(value);
            root = node;
            nodesCount++;
            // System.out.println("root");
            callObserver(node, value, NodeModificationObserver.Action.New);
            return;
        }
        Node currNode = root;
        while (true) {
            if (currNode.key == key) {
                throw new DuplicateValueException();
            }
            if (currNode.key.compareTo(key) > 0) { // go left
                if (currNode.left == null) {
                    Node node = new Node();
                    node.key = key;
                    node.valuesSet.add(value);
                    root = node;
                    nodesCount++;
                    //System.out.println(nodesCount);
                    callObserver(node, value, NodeModificationObserver.Action.New);
                    return;
                } else { // there is a left node
                    currNode = currNode.left;
                    // continue the loop
                }
            } else { // go right
                if (currNode.right == null) {
                    Node node = new Node();
                    node.key = key;
                    node.valuesSet.add(value);
                    root = node;
                    nodesCount++;
                    //System.out.println(nodesCount);
                    callObserver(node, value, NodeModificationObserver.Action.New);
                    return;
                } else { // there is a right node
                    currNode = currNode.right;
                    // continue the loop
                }
            }
        }
    }

    /**
     * ************************************************************************************
     */
    boolean containsKey(String key) {
        Node node = root;
        Node parent = null;
        while (node != null) {
            if (key == node.key) {
                return true;
            }
            if (key.compareTo(node.key) > 0) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;
            } else {//GO RIGHT 
                parent = node;
                node = node.right;
            }
        }
        return false;
    }

    /**
     * ************************************************************************************
     */
    ArrayList<Integer> getValuesByKey(String key) {
        Node node = root;
        ArrayList list = null;
        Node parent = null;
        while (node != null) {
            if (key == node.key) {
                for (Integer i : node.valuesSet) {
                    list.add(i);
                }

            }
            if (key.compareTo(node.key) > 0) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;
            } else {//GO RIGHT 
                parent = node;
                node = node.right;
            }
        }
        return list;
    }
    private Pair[] tree;
    private int treeIndex;

    private void collectInOrder(Node node) {
        if (node == null) {
            //System.out.println("get");
            return;
        }
        collectInOrder(node.left);
        tree[treeIndex++].key = node.key;
        tree[treeIndex++].value = node.valuesSet;
        collectInOrder(node.right);
    }

    Pair[] getInOrder() {
        tree = new Pair[nodesCount];
        //System.out.println("get");
        treeIndex = 0;
        System.out.println(root.key);
        collectInOrder(root);
        //System.out.println("added");
        return tree;
    }

    ArrayList<String> getKeysContainingValue(int value) {
        Pair[] tree = getInOrder();
        ArrayList keys = null;
        for (Pair p : tree) {
            if ((int) (p.value) == value) {
                keys.add(p.key);
            }
        }
        /*ArrayList keys = null;
            Node node = root;
            ArrayList list = null;
            Node parent = null;
            while (node != null) {
                for (int i : node.valuesSet) {
                    if (value == i) {
                        keys.add(node.key);
                    }
                    if (i) {//GREATER THAN?GO LEFT
                        parent = node;
                        node = node.left;
                    } else {//GO RIGHT 
                        parent = node;
                        node = node.right;
                    }
                }
            }*/

        return keys;
    }

    /**
     * ************************************************************************************
     */
    @Override
    public String toString() {
        String output = null;
        //System.out.println("added");
        Pair[] tree = getInOrder();
        ArrayList keys = null;
        for (Pair p : tree) {
            output += p.key + ":" + p.value;
        }
        return output;
    }

    /**
     * ************************************************************************************
     */

    ArrayList<String> getAllKeys() {
        Pair[] tree = getInOrder();
        ArrayList keys = null;
        for (Pair p : tree) {

            keys.add(p.key);

        }
        return keys;
    }
    /**
     * ************************************************************************************
     */

}

public class FinalTreePatterns {

    public static void main(String[] args) {
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.addObserver((node, value, action) -> {
            try (PrintWriter fileOutput = new PrintWriter("events.txt")) {
            fileOutput.println("Event:" + action + " ,nodeKey=" + node.key + " ,value=" + value);
        } catch (IOException ex) {
           Logger.getLogger(FinalTreePatterns.class.getName()).log(Level.SEVERE, null, ex); 
        } 
        });
        try {
            tree.add("first", 0);
            tree.add("second", 0);
            /*for (Pair pair : tree) {
                System.out.print(v + ", ");
            }*/
        } catch (DuplicateValueException ex) {
            Logger.getLogger(FinalTreePatterns.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
