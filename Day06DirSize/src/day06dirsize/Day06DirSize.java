/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package day06dirsize;

import java.io.File;

/**
 *
 * @author 6155202
 */
public class Day06DirSize {

   static long getDirTotalSizeInByte(File directory){
   long totalSize=0;
   File[]entryArray=directory.listFiles();
   if(entryArray==null){
       System.out.println("Warning:unable to enter"+directory.getAbsolutePath());
       return 0;
   }
   for(File entry:entryArray){
       if(entry.isFile()){
           totalSize+=entry.length();
           
       }
       else if (entry.isDirectory()){
           //System.out.println("Entering directory:"+entry.getAbsolutePath());
           totalSize+=getDirTotalSizeInByte(entry);
       }
   }
   return totalSize;

}
    public static void main(String[] args) {
        long size=getDirTotalSizeInByte(new File("C:\\Program Files\\Java\\jdk-13.0.2"));
        System.out.printf("%,d",size);
    }

}
