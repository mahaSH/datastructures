/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06bighomework;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author AQ-37
 */
public class Birthday {

    private String name;
    private Date bd;
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    //constructor1
    public Birthday(String name, Date bd) {
        setName(name);
        setBd(bd);

    }
    //from line constructor
    //Setters and Getters
    public String getName() {
        return name;

    }

    public void setName(String name) {
        if(name.length()<1||name.length()>50){
            System.out.println("name if invalid");
            return;
        }
        this.name = name;
    }

    public Date getBd() {
        return bd;
    }

    public void setBd(Date bd) {
        this.bd = bd;
    }

    /*  public int daysTillBD(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(bd);
        int month = cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);

    }*/

    //OverRidden toString
    @Override
    public String toString() {
        return String.format("%s born in %s birthday indays", name, dateFormat.format(bd));
    }
    //toDataString function

    String toDataString() {
        return String.format("%s;%d", name, dateFormat.format(bd));
    }

    //comparators
    final static Comparator<Birthday> comparatorByName = new Comparator<Birthday>() {
        @Override
        public int compare(Birthday p1, Birthday p2) {
            return p1.getName().compareTo(p2.getName());
        }
    };

}
