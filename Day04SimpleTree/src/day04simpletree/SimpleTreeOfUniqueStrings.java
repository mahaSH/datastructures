/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04simpletree;

/**
 *
 * @author AQ-37
 */
public class SimpleTreeOfUniqueStrings {

    private class Node {

        int value;
        Node left, right, parent;

        private Node(int value, Object object, Object object0, Object object1) {
            this.value = value;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }
    }
    private Node root;
    private int nodesTotal = 0;

    public void add(int value) {// throws IllegalArgumentException if value already present
        if (nodesTotal == 0) {//1-IF THE TREE IS EMPTY:
            Node node = new Node(value, null, null, null);
            root = node;
            nodesTotal++;
            return;
        } //2-CHECK IF THE VALUE ALREADY EXIST:
          /*  Node temporaryRoot = root;
            do {System.out.println(value);
                if (temporaryRoot.value == value) {
                    throw new IllegalArgumentException("VALUE ALREADY PRESENT");
                } else if (value > temporaryRoot.value && temporaryRoot.right != null) {//IF THE VALUE IS GREATER THAN THE NODE VALUE:CHECK THE RIGHT
                    temporaryRoot = temporaryRoot.right;
                    System.out.println(7);
                    if (value == temporaryRoot.value) {
                        throw new IllegalArgumentException("VALUE ALREADY PRESENT");
                    }
                } else if (value < temporaryRoot.value && temporaryRoot.left != null) {//IF THE VALUE IS SMALLER THAN THE NODE VALUE:CHECK THE LEFT
                    temporaryRoot = temporaryRoot.left;
                    System.out.println(6);
                    System.out.println(temporaryRoot.value);
                    if (value == temporaryRoot.value) {
                        throw new IllegalArgumentException("VALUE ALREADY PRESENT");
                    }
                }

            } while (temporaryRoot != null);//NO EXCEPTIONS FOUND:THE VALUE IS NOT DUPLICATED,ADD IT*/
            Node temporaryRootAdding = root;//3-FIND THE CORRECT POSITION OF THE NEW NODE,CREATE IT, AND INSERT IT.
            while (temporaryRootAdding != null) {
                if (value > temporaryRootAdding.value) {//IF THE VALUE IS GREATER THAN THE NODE VALUE:GO TO THE RIGHT
                    temporaryRootAdding = temporaryRootAdding.right;
                } else {//IF THE VALUE IS SMALLER THAN THE NODE VALUE:GO TO THE LEFT
                    temporaryRootAdding = temporaryRootAdding.left;

                }
            }//NOW,THE TEMPORARYROOTADDING VARIABLE CONTAINSTHE CORRECT POSITION OF THE NEW NODE.
            Node node = new Node(value, null, null, temporaryRootAdding);//CREATE THE NEW NODE,AND INSERT IT IN THE TREE
            nodesTotal++;
        
    }

    public boolean has(int value) {
        Node temporaryRoot = root;
        while (temporaryRoot!= null){
            if (value == temporaryRoot.value) {
                throw new IllegalArgumentException("VALUE ALREADY PRESENT");
            } else if (value > temporaryRoot.value&&temporaryRoot.right!=null) {//IF THE VALUE IS GREATER THAN THE NODE VALUE:GO TO THE RIGHT
                temporaryRoot = temporaryRoot.right;
                if (temporaryRoot.value==value) {
                    throw new IllegalArgumentException("VALUE ALREADY PRESENT");
                }
            } else if (value < temporaryRoot.value&&temporaryRoot.left!=null){//IF THE VALUE IS SMALLER THAN THE NODE VALUE:GO TO THE LEFT
                temporaryRoot = temporaryRoot.left;
                if (temporaryRoot.value==value&&temporaryRoot.left!=null) {
                    throw new IllegalArgumentException("VALUE ALREADY PRESENT");
                }
            }
        }
        return true;
    }

    public void remove(int value) {
        throw new UnsupportedOperationException("operaton not supported yet");
    } // HARD! throws IllegalArgumentException if value does not exist

    public int size() {
        return nodesTotal;
    }

    public int[] toArray() {
        int[] nodes = new int[nodesTotal];
        int counter = 0;
        Node temporaryRoot = root;
        while (temporaryRoot.left != null) {
            temporaryRoot = temporaryRoot.left;
        }
        nodes[counter] = temporaryRoot.value;
       
counter++;//LAST ELEMNT IN LEFT SIDE FOUND,ADD IT TO THE ARRAY
        while (temporaryRoot != root) {//GIVE THE LEFTSIDE OF THE TREE TO THE ARRAY
            temporaryRoot = temporaryRoot.parent;
            nodes[counter] = temporaryRoot.value;
            counter++;
            temporaryRoot = temporaryRoot.right;
            nodes[counter++] = temporaryRoot.value;
            counter++;
        }
        nodes[counter] = root.value;//GINE THE ROOT VALUE TO THE ARRAY
        counter++;
        temporaryRoot = root.right;
        while (temporaryRoot != null) {//GIVE THE RIGHTSIDE OF THE TREE TO THE ARRAY
            temporaryRoot = temporaryRoot.left;
            nodes[counter] = temporaryRoot.value;
            counter++;
        }
        while (temporaryRoot  != root&&temporaryRoot  !=null) {//GIVE THE VERY RIGHTSIDE OF THE TREE TO THE ARRAY
            temporaryRoot = temporaryRoot.parent;
            temporaryRoot = temporaryRoot.right;
            nodes[counter] = temporaryRoot.value;
            counter++;
        }
        return nodes;
    } // allocate array of nodesTotal, recursively visit all nodes of the tree

    public int[] toArraySorted() {
        throw new UnsupportedOperationException("operaton not supported yet");
    } // HARD! Traverse from left-most low to right-most low

    public void printDebug() {
    } // any output that helps you with debugging, recursively visit all nodes of the trees

    @Override
    public String toString() {
        throw new UnsupportedOperationException("operaton not supported yet");
    } // result like for ArrayList: "[Value1,Value2,Value3]"
}
