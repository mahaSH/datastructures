/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da06treeagain;

import java.util.ArrayList;

class duplicateValueException extends Exception {

    public duplicateValueException() {
    }
}

public class TreeAgain {

    class NodeOfInt {

        NodeOfInt left, right, parent;
        int value;
    }
    NodeOfInt root = new NodeOfInt();
    int nodesCount = 0;

    void put(int value) throws duplicateValueException {
        if (nodesCount == 0) {
            root.value = value;
            root.left = null;
            root.right = null;
            root.parent = null;
            nodesCount++;
            return;

        } else {
            //IF NOT THE FIRST NODE IN THE TREE
            NodeOfInt node = root;
            NodeOfInt parent = null;
            int side = 0;
            while (node != null) {
                if (value == node.value) {//IF REPEATED VALUE:THROW AN EXCEPTION
                    throw new duplicateValueException();
                }
                if (value < node.value) {//GREATER THAN?GO LEFT
                    parent = node;
                    node = node.left;
                    side = 1;
                } else {//GO RIGHT 
                    parent = node;
                    node = node.right;
                    side = 2;
                }
            }
            NodeOfInt newNode = new NodeOfInt();
            newNode.left = null;
            newNode.right = null;
            newNode.parent = parent;
            newNode.value = value;
            if (side == 1) {
                parent.left = newNode;
            } else {
                parent.right = newNode;
            }
            nodesCount++;

        }
    }

    int getSumOfAllValues(NodeOfInt node) {
        if (node == null) {
            return 0;
        }
        int left = getSumOfAllValues(node .left);
        int right = getSumOfAllValues(node.right);
        return (node.value + left + right);

    }

    boolean hasValue(int value) {
        NodeOfInt node = root;
        NodeOfInt parent = null;
        while (node != null) {
            if (value == node.value) {//IF REPEATED VALUE:THROW AN EXCEPTION
                return true;
            }
            if (value < node.value) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;
            } else {//GO RIGHT 
                parent = node;
                node = node.right;
            }
        }
        return false;
    }

    boolean remove(int value) throws duplicateValueException {
        ArrayList<NodeOfInt> subValues = new ArrayList<>();
        NodeOfInt node = root;
        //1-FIND THE DESIRED NODE
        NodeOfInt parent = null;
        int side = 0;
        while (node != null) {
            if (value == node.value) {//DELETE THE NODE IF FOUND
                if(node.left==null&&node.right==null){
                    
                }
                NodeOfInt currentNode=node;
                 node = null;
                while (currentNode != null) {//LOOP IN THE SUBTREE
                    if (value < currentNode.value) {//GREATER THAN?GO LEFT
                        parent = currentNode;
                        currentNode= currentNode.left;
                        subValues.add(currentNode);
                        currentNode = null;
                    } else {//GO RIGHT 
                        parent = currentNode;
                        currentNode = currentNode.right;
                        subValues.add(currentNode);
                        currentNode = null;
                    }//END OF COUNTING WHILE
                    for (int i = 0; i < subValues.size(); i++) {
                        put(subValues.get(i).value);
                    }
                }
                nodesCount--;
            }
                if (value < node.value) {//GREATER THAN?GO LEFT
                    parent = node;
                    node = node.left;
                    side = 1;
                } else {//GO RIGHT 
                    parent = node;
                    node = node.right;
                    side = 2;
                }
        }
            return false;

        }
}
