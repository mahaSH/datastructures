package day02wordsfreq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

 class Day02WordsFreq {

    static HashMap<String, Integer> wordFreqMap = new HashMap<>();

    public static void main(String[] args) {
        long startTimeTotal = System.currentTimeMillis();
        
        try ( Scanner fileInput = new Scanner(new File("textFull.txt"))) {
            fileInput.useDelimiter("[ \t\n\r\\.,:;-]");
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                if (word.isEmpty()) {
                    continue;
                }
                wordFreqMap.putIfAbsent(word, 0);
                int freq = wordFreqMap.get(word);
                wordFreqMap.put(word, freq + 1);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Day02WordsFreq.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (String word : new String[]{"God", "LORD", "called", "blessing"}) {
            System.out.printf("Word %s occures %d times\n", word, wordFreqMap.get(word));
        }
        ArrayList<Integer> maxOccure = new ArrayList<>();        
        
        for (Integer i : wordFreqMap.values()) {
            maxOccure.add(i);
        }
        Collections.sort(maxOccure, comparatorInt);
        //System.out.println("maxOccure= " + maxOccure.toString());
        System.out.println("Top 10 occures word:");
        for (int i = 0; i < 10; i++) {
            Integer value = maxOccure.get(i);            
            for (String w : wordFreqMap.keySet()) {                
                if (wordFreqMap.get(w) == value) {
                    System.out.println("Word: '" + w + "' occurs: " + wordFreqMap.get(w));
                }
            }
        }
        
        long endTimeTotal = System.currentTimeMillis();
        double durationTotal = (endTimeTotal - startTimeTotal) / 1000.0;
        System.out.printf("\nTotal runtime is %.3fs\n", durationTotal);

    }

    final static Comparator<Integer> comparatorInt = new Comparator<Integer>() {
        @Override
        public int compare(Integer p1, Integer p2) {
            return p2 - p1;
        }
    };

}
