/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07firstfractal;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author 6155202
 */
public class fractalsPanel extends javax.swing.JPanel {

    /**
     * Creates new form fractalsPanel
     */
    public fractalsPanel() {
        // initComponents();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 100);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);//Has To Stay..Never Remove It
        Graphics2D g2d = (Graphics2D) g.create();
        drawFractal(g2d, 0, 600, 10);
        //drawFractal(g2d, 0, 200, 20);
        

    }

    private void drawFractal(Graphics2D g2d, int fromX, int toX, int levelY) {
        if (levelY >100) {
            return;
        }
        int totalWidth = toX - fromX;
        g2d.drawLine(fromX, levelY, fromX + totalWidth / 3, levelY);
         //System.out.printf("(%d,%d)(%d,%d)\n",fromX, levelY, fromX + totalWidth / 3, levelY);
        g2d.drawLine(toX - totalWidth / 3, levelY, toX, levelY);
       // System.out.printf("(%d,%d)(%d,%d)\n",toX - totalWidth / 3, levelY, toX, levelY);
        //recursive
       drawFractal(g2d,fromX,fromX+totalWidth/3,levelY+15);
       drawFractal(g2d,toX-totalWidth/3,toX,levelY+15);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        canvas1 = new java.awt.Canvas();

        setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(canvas1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(canvas1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Canvas canvas1;
    // End of variables declaration//GEN-END:variables
}
