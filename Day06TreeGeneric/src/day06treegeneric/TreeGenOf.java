/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06treegeneric;

class DuplicateKeyException extends Exception {

    public DuplicateKeyException() {
    }
}

public class TreeGenOf<K extends Comparable<K>, V extends Comparable<V>> {

    private class Node<K, V> {

        Node<K, V> left, right, parent;
        K key;
        V value;
    }

    private Node<K, V> root;
    private int nodesCount;

    void put(K key, V value) throws DuplicateKeyException {
        if (nodesCount == 0) {//IF THE TREE IS EMPTY:PUT THE FIRST NODE IN THE TREE
            root.value = value;
            root.left = null;
            root.right = null;
            root.parent = null;
            root.key = key;
            nodesCount++;
            return;
        } //ELSE,FIND THE SUITABLE PLACE TO PUT THE NEW NODE
        Node<K, V> node = root;
        Node<K, V> parent = null;
        String side = null;
        while (node != null) {
            if (key == node.key) {//IF REPEATED Key:THROW AN EXCEPTION
                throw new DuplicateKeyException();
            }
            if (key.compareTo(node.key) > 0) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;
                side = "left";
            } else {//GO RIGHT 
                parent = node;
                node = node.right;
                side = "right";
            }
        }
        Node<K, V> newNode = new Node<K, V>();
        newNode.left = null;
        newNode.right = null;
        newNode.parent = parent;
        newNode.value = value;
        if (side == "left") {
            parent.left = newNode;
        } else {
            parent.right = newNode;
        }
        nodesCount++;

    }

    private void collectValuesInOrder(Node<K, V> node) {
        if (node == null) {
            return;
        }
        collectValuesInOrder(node.left);
        resultArray[resultIndex++] = node.key;
        collectValuesInOrder(node.right);
    }
    private K[] resultArray;
    private int resultIndex;

    K[] getListOfAllKeysInOrderOfKeys() {
        resultArray = (K[]) new Comparable[nodesCount];
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }
    private V[] valuesArray;

    private void collectKeysInOrder(Node<K, V> node) {
        if (node == null) {
            return;
        }
        collectValuesInOrder(node.left);
        valuesArray[resultIndex++] = node.value;
        collectValuesInOrder(node.right);
    }

    V[] getListOfAllValuesInOrderOfKeys() {
        valuesArray = (V[]) new Comparable[nodesCount];
        resultIndex = 0;
        collectKeysInOrder(root);
        return valuesArray;
    }

    boolean hasKey(K key) {
        Node<K, V> node = root;
        Node<K, V> parent = null;
        while (node != null) {
            if (key.compareTo(node.key) == 0) {//IF REPEATED Key:THROW AN EXCEPTION
                return true;
            }
            if (key.compareTo(node.key) > 0) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;
            } else {//GO RIGHT 
                parent = node;
                node = node.right;
            }
        }
        return false;
    }

    boolean hasValue(V value) {
        Node<K, V> node = root;
        Node<K, V> parent = null;
        while (node != null) {
            if (value.compareTo(node.value) == 0) {
                return true;
            }
            if (value.compareTo(node.value) > 0) {//GREATER THAN?GO LEFT
                parent = node;
                node = node.left;

            } else {//GO RIGHT 
                parent = node;
                node = node.right;
            }
        }
        return false;
    }

    void removeByKey(K key) {
    } // Difficult

    /*void boolean removeByValue(V value) {
     throw new UnsupportedOperationException();
    } // Difficult, remove all nodes matching value*/
    int getNodesCount() {
        return nodesCount;
    }
}
